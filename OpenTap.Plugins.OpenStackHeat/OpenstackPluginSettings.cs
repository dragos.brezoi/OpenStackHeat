﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

namespace OpenTap.Plugins.OpenStackHeat
{
    /// <summary>
    /// Settings for Open Stack Plugin
    /// </summary>
    [Display("Openstack Plugin")]
    public sealed class OpenstackPluginSettings : ComponentSettings<OpenstackPluginSettings>
    {
        /// <summary>
        /// REST API Call Timeout
        /// </summary>
        [Display("Rest Call Timeout")]
        public double RestCallTimeout { get; set; }

        /// <summary>
        /// Creates a new instance of <see cref="OpenstackPluginSettings">OpenstackPluginSettings</see> class.
        /// </summary>
        public OpenstackPluginSettings()
        {
            RestCallTimeout = 10000; // ms
        }
    }
}
