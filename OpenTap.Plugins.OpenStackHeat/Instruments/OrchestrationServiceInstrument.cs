﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Objects;
using OpenTap.Plugins.OpenStackHeat.RestClients;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackHeat.Instruments
{
    [Display(Name: "Orchestration Service", Description: "The OpenStack's Orchestration Service provides a template-based orchestration for describing a cloud application by running OpenStack API calls to generate running cloud applications. The software integrates other core components of OpenStack into a one-file template system. The templates allow you to create most OpenStack resource types, such as instances, floating IPs, volumes, security groups and users.", Groups: new string[] {"Heat"})]
    public sealed class OrchestrationServiceInstrument : Instrument
    {
        #region Settings 

        /// <summary>
        /// Orchestration Service Endpoint
        /// </summary>
        [Display(Name: "Orchestration Service Endpoint", Group: "Settings")]
        public string Endpoint { get; set; }

        /// <summary>
        /// Identity Service Instrument
        /// </summary>
        [ResourceOpen(ResourceOpenBehavior.Before)]
        [Display(Name: "Identity Service Instrument", Group: "Settings")]
        public IdentityServiceInstrument IdentityService { get; set; }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="OrchestrationServiceInstrument">OrchestrationServiceInstrument</see> class.
        /// </summary>
        public OrchestrationServiceInstrument()
        {
            Name = "Orchestration Service";
            Endpoint = "http://default.orchestration:8004";

            Rules.Add(() => !string.IsNullOrEmpty(Endpoint), "An endpoint is required.", "Endpoint");
            Rules.Add(() => IdentityService != null, "An Identity Service is required.", "IdentityService");
        }

        /// <summary>
        /// Closes the connection to the Identity Service
        /// </summary>
        public override void Close()
        {
            base.Close();
        }

        /// <summary>
        /// Opens the connection to the Identity Service
        /// </summary>
        /// <exception cref="System.Exception">Execution cannot continue</exception>
        public override void Open()
        {
            base.Open();

            Log.Debug("// Version Handshake -------------------------------------------------------------------------------------");
            Task<RestClientResult<List<Version>>> taskGetVersions = OrchestrationClient.GetVersions(Endpoint);
            taskGetVersions.Wait();
            List<Version> versions = taskGetVersions.Result.Result;

            if (versions == null)
            {
                Log.Error($"Could not fetch API versions from EndPoint {Endpoint}! Details: {taskGetVersions.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Version foundVersion = null;
                foreach (Version version in versions)
                {
                    if (version.Id == "v1.0" && version.Status == "CURRENT")
                    {
                        foundVersion = version;
                        break;
                    }
                }
                if (foundVersion != null)
                {
                    Log.Info(foundVersion.ToString());
                }
                else
                {
                    Log.Error($"Unknown REST EndPoint {Endpoint}!");
                    foreach (Version version in versions)
                    {
                        Log.Error(versions.ToString());
                    }
                    throw new System.Exception("Execution cannot continue");
                }
            }
            Log.Debug("----------------------------------------------------------------------------------------------------------");
        }

        /// <summary>
        /// Lists active stacks.
        /// </summary>
        public void ListStacks()
        {
            Log.Debug("-- List Stacks --------------------------------------------------------------------------------------------");
            Task<RestClientResult<string>> taskListStacks = OrchestrationClient.ListStacks(Endpoint, IdentityService.ProjectId, IdentityService.Token);
            taskListStacks.Wait();
            string stacks = taskListStacks.Result.Result;
            if (stacks == null)
            {
                Log.Error($"List Stacks failed! Details: {taskListStacks.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Log.Debug("List Stacks executed succesfully!");
            }
            Log.Info(stacks);
            Log.Debug("----------------------------------------------------------------------------------------------------------");
        }

        /// <summary>
        /// Creates a stack.
        /// </summary>
        /// <param name="stackName">The name of the stack to be created.</param>
        /// <param name="hotContentAsBase64">The Heat Orchestration Template File to be loaded as base64.</param>
        /// <param name="timeout">Timeout this operation after the given amount of time (expressed in seconds).</param>
        /// <returns>The newly created stack details</returns>
        public Stack CreateStack(string stackName, string hotContentAsBase64, double timeout)
        {
            Stack stack = null;
            Log.Debug("-- Create Stack -------------------------------------------------------------------------------------------");
            Task<RestClientResult<Stack>> taskCreateStack = OrchestrationClient.CreateStack(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, hotContentAsBase64);
            taskCreateStack.Wait();
            stack = taskCreateStack.Result.Result;
            if (stack == null)
            {
                Log.Error($"The Stack was not created! Details: {taskCreateStack.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Log.Info("Stack Creation scheduled succesfully!");

                List<string> expectedStatuses = new List<string>
                {
                    //"CREATE_IN_PROGRESS",
                    "CREATE_FAILED",
                    "CREATE_COMPLETE"
                };

                var sw = System.Diagnostics.Stopwatch.StartNew();
                while (true)
                {
                    Task<RestClientResult<List<ResourceEvent>>> taskListStackEvents = OrchestrationClient.ListStackEvents(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, stack.Id);
                    List<ResourceEvent> Events = taskListStackEvents.Result.Result;

                    IEnumerable<ResourceEvent> interestingEvents =  Events.Where(x => x.ResourceName == stackName && expectedStatuses.Contains(x.ResourceStatus));

                    if (interestingEvents.Count() > 0)
                    {
                        var createFailedEvent = interestingEvents.Where(x => x.ResourceName == stackName && x.ResourceStatus == "CREATE_FAILED").FirstOrDefault();
                        if(createFailedEvent != null)
                        {
                            Log.Error($"Stack {stackName} creation failed! Details: {createFailedEvent.ResourceStatusReason}");
                        }
                        else // must be CREATE_COMPLETE 
                        {
                            Log.Info($"Stack {stackName} creation was succesful! Id: {stack.Id}");
                        }

                        // get the full details
                        Task<RestClientResult<Stack>> taskShowStackDetails = OrchestrationClient.ShowStackDetails(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, stack.Id);
                        if (taskShowStackDetails.Result.Result != null)
                        {
                            stack = taskShowStackDetails.Result.Result;
                        }

                        break;
                    }
                    else if (sw.Elapsed.TotalSeconds > timeout)
                    {
                        // timed-out
                        Log.Error($"Stack {stackName} creation timed-out!");
                        stack.StackName = stackName;
                        break;
                    }
                    else
                    {
                        TapThread.Sleep(500);
                    }
                }
            }
            Log.Debug("----------------------------------------------------------------------------------------------------------");
            return stack;
        }

        /// <summary>
        /// Deletes a stack. If a stack has snapshots, those snapshots are deleted as well.
        /// </summary>
        /// <param name="stackName">The name of the Stack.</param>
        /// <param name="stackId">The Id of the Stack.</param>
        /// <param name="timeout">Timeout this operation after the given amount of time (expressed in seconds).</param>
        /// <returns>A boolean indicating whether the stack has been deleted or not.</returns>
        public bool DeleteStack(string stackName, string stackId, double timeout)
        {
            bool deleted = false;
            Log.Debug("-- Delete stacks ------------------------------------------------------------------------------------------");
            Task<RestClientResult<bool>> taskDeleteStack = OrchestrationClient.DeleteStack(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, stackId);
            taskDeleteStack.Wait();
            deleted = taskDeleteStack.Result.Result;
            if (deleted == false)
            {
                Log.Error($"The Stack was not deleted! Details: {taskDeleteStack.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Log.Info("Stack Deletion scheduled successfully!");

                List<string> expectedStatuses = new List<string>
                {
                    //"DELETE_IN_PROGRESS",
                    "DELETE_FAILED",
                    "DELETE_COMPLETE"
                };

                var sw = System.Diagnostics.Stopwatch.StartNew();
                while (true)
                {
                    Task<RestClientResult<List<ResourceEvent>>> taskListStackEvents = OrchestrationClient.ListStackEvents(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, stackId);
                    List<ResourceEvent> Events = taskListStackEvents.Result.Result;

                    IEnumerable<ResourceEvent> interestingEvents = Events.Where(x => x.ResourceName == stackName && expectedStatuses.Contains(x.ResourceStatus));

                    if (interestingEvents.Count() > 0)
                    {
                        var deleteFailedEvent = interestingEvents.Where(x => x.ResourceName == stackName && x.ResourceStatus == "DELETE_FAILED").FirstOrDefault();
                        if (deleteFailedEvent != null)
                        {
                            Log.Error($"Stack {stackName} deletion failed! Details: {deleteFailedEvent.ResourceStatusReason}");
                            deleted = false;
                        }
                        else // must be DELETE_COMPLETE 
                        {
                            Log.Info($"Stack {stackName} deletion was succesful! Id: {stackId}");
                            deleted = true;
                        }
                        break;
                    }
                    else if (sw.Elapsed.TotalSeconds > timeout)
                    {
                        // timed-out
                        Log.Error($"Stack {stackName} deletion timed-out!");
                        deleted = false;
                        break;
                    }
                    else
                    {
                        TapThread.Sleep(500);
                    }
                }
            }
            Log.Debug("----------------------------------------------------------------------------------------------------------");
            return deleted;
        }

        /// <summary>
        /// Lists outputs for a stack.
        /// </summary>
        /// <param name="stackName">The name of a stack.</param>
        /// <param name="stackId">The UUID of the stack.</param>
        /// <returns>The output of the stack printed in the log in a tabular form.</returns>
        public void ListOutputs(string stackName, string stackId)
        {
            Log.Debug("-- List Outputs -------------------------------------------------------------------------------------------");
            Task<RestClientResult<List<StackOutput>>> taskListOutputs = OrchestrationClient.ListOutputs(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, stackId);
            taskListOutputs.Wait();
            List<StackOutput> outputs = taskListOutputs.Result.Result;
            if (outputs == null)
            {
                Log.Error($"List Outputs failed! Details: {taskListOutputs.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Log.Debug("List Outputs executed succesfully!");

                int maxWidthName = 0;
                int maxWidthValue = 0;
                int maxWidthDescription = 0;

                foreach (StackOutput output in outputs)
                {
                    StackOutput outputWithValue = ShowOutput(stackName, stackId, output.Key);
                    output.Value = outputWithValue?.Value;

                    int widthName = output.Key != null ? output.Key.Length : 0;
                    int widthValue = output.Value != null ? output.Value.Length : 0;
                    int widthDescription = output.Description != null ? output.Description.Length : 0;

                    maxWidthName = widthName > maxWidthName ? widthName : maxWidthName;
                    maxWidthValue = widthValue > maxWidthValue ? widthValue : maxWidthValue;
                    maxWidthDescription = widthDescription > maxWidthDescription ? widthDescription : maxWidthDescription;
                }


                string formatHeader = string.Format("+{{0,-{0}}}+{{1,-{1}}}+{{2,-{2}}}+", maxWidthName, maxWidthValue, maxWidthDescription);
                string formatContent = string.Format("|{{0,-{0}}}|{{1,-{1}}}|{{2,-{2}}}|", maxWidthName, maxWidthValue, maxWidthDescription);

                string headerLine = string.Format(formatHeader, new string('-', maxWidthName), new string('-', maxWidthValue), new string('-', maxWidthDescription));

                Log.Info(headerLine);
                Log.Info(string.Format(formatContent, "Name", "Value", "Description"));
                Log.Info(headerLine);
                foreach (StackOutput output in outputs)
                {
                    Log.Info(string.Format(formatContent, output.Key, output.Value, output.Description));
                }
                Log.Info(headerLine);
            }
            Log.Debug("----------------------------------------------------------------------------------------------------------");
        }

        /// <summary>
        /// Shows details for a stack output.
        /// </summary>
        /// <param name="stackName">The name of a stack.</param>
        /// <param name="stackId">The UUID of the stack.</param>
        /// <param name="key">The key of a stack output</param>
        /// <returns>The stack output.</returns>
        public StackOutput ShowOutput(string stackName, string stackId, string outputKey)
        {
            StackOutput output = null;
                 
            Log.Debug("-- Show Output -------------------------------------------------------------------------------------------");
            Task<RestClientResult<StackOutput>> taskShowOutput = OrchestrationClient.ShowOutput(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, stackId, outputKey);
            taskShowOutput.Wait();
            output = taskShowOutput.Result.Result;
            if (output == null)
            {
                Log.Error($"Show Output failed! Details: {taskShowOutput.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                Log.Debug("Show Output executed succesfully!");
            }

            Log.Info(output.ToString());
            Log.Debug("----------------------------------------------------------------------------------------------------------");

            return output;
        }

        /// <summary>
        /// Lists events for a stack.
        /// </summary>
        /// <param name="stackName">The name of a stack.</param>
        /// <param name="stackId">The UUID of the stack.</param>
        /// <returns>A list of occured events for the given stack.</returns>
        public List<ResourceEvent> ListStackEvents(string stackName, string stackId)
        {
            List<ResourceEvent> events;

            //Log.Debug("// List Stack Events -------------------------------------------------------------------------------------");
            Task<RestClientResult<List<ResourceEvent>>> taskListStackEvents = OrchestrationClient.ListStackEvents(Endpoint, IdentityService.ProjectId, IdentityService.Token, stackName, stackId);
            taskListStackEvents.Wait();
            events = taskListStackEvents.Result.Result;
            if (events == null)
            {
                Log.Error($"List Stack Events failed! Details: {taskListStackEvents.Result.Message}");
                throw new System.Exception("Execution cannot continue");
            }
            else
            {
                //Log.Debug("List Stack Events executed succesfully!");
            }
            //Log.Debug("----------------------------------------------------------------------------------------------------------");

            return events;
        }
    }
}
