﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;
using OpenTap.Plugins.OpenStackHeat.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display(Name: "Wait For Stack Event", Description: "Blocks the execution until the given stack event or a timeout occurs.", Group: "Heat")]
    public sealed class WaitForStackEventStep : TestStep
    {
        #region Settings

        private double timeout = 600;

        /// <summary>
        /// Orchestration Service
        /// </summary>
        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        /// <summary>
        /// The stack to list events for
        /// </summary>
        [Display(Name: "Stack", Description: "The stack to list events for", Group: "Context")]
        public Input<Stack> Stack { get; set; }

        /// <summary>
        /// Event Name to wait for.
        /// </summary>
        [Display(Name: "Status", Description: "Event Name to wait for.", Group: "Settings")]
        [AvailableValues("StatusValues")]
        public string StackStatus { get; set; }

        /// <summary>
        /// The timeout to end executing after.
        /// </summary>
        [Unit("s")]
        [Display(Name: "Timeout", Description: "The timeout to end executing after.", Group: "Settings")]
        public double Timeout
        {
            get { return timeout; }
            set
            {
                if (value >= 0)
                    timeout = value;
                else throw new Exception("Timeout must be positive");
            }
        }

        public IEnumerable<string> StatusValues
        {
            get
            {
                List<string> statusValues = new List<string>();
                statusValues.Add("CREATE_IN_PROGRESS");
                statusValues.Add("CREATE_FAILED");
                statusValues.Add("CREATE_COMPLETE");
                statusValues.Add("DELETE_IN_PROGRESS");
                statusValues.Add("DELETE_COMPLETE");
                statusValues.Add("DELETE_FAILED");
                statusValues.Add("FAILED");
                statusValues.Add("TIMEOUT");
                statusValues.Add("UNDEFINED");
                statusValues.Add("UNKNOWN");
                return statusValues;
            }
            }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="WaitForStackEventStep">WaitForStackEventStep</see> class.
        /// </summary>
        public WaitForStackEventStep()
        {
            Name = "Wait For '{Status}'";
            Stack = new Input<Stack>();

            Rules.Add(() => string.IsNullOrEmpty(StackStatus), "The name of the event cannot be null.");
            Rules.Add(() => Stack.Step != null, "A stack is required.", "Stack");
            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();
            while (true)
            {
                List<ResourceEvent> Events = OrchestrationService.ListStackEvents(Stack.Value.StackName, Stack.Value.Id);
                if (Events.Where(x => x.ResourceName == Stack.Value.StackName && x.ResourceStatus == StackStatus).Count() > 0)
                {
                    // event arrived
                    UpgradeVerdict(Verdict.Pass);
                    break;
                }
                else if (sw.Elapsed.TotalSeconds > this.Timeout)
                {
                    // timed-out
                    UpgradeVerdict(Verdict.Fail);
                    break;
                }
                else
                {
                    TapThread.Sleep(100);
                }
            }
        }
    }
}
