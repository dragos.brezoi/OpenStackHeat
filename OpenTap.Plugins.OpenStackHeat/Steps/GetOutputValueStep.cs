﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;
using OpenTap.Plugins.OpenStackHeat.Objects;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display("Get Output Value", Group: "Heat", Description: "Gets the value of a given stack's output.")]
    public sealed class GetOutputValueStep : TestStep
    {
        #region Settings

        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        [Display(Name: "Key", Description: "The name of the given output", Group: "Settings")]
        public string Key { get; set; }

        [Display(Name: "Value", Description: "The value of the given output", Group: "Output")]
        [Output]
        public string Value { get; set; }

        [Display(Name: "Stack", Description: "The stack to which the output belongs", Group: "Context")]
        public Input<Stack> Stack { get; set; }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="GetOutputValueStep">GetOutputValueStep</see> class.
        /// </summary>
        public GetOutputValueStep()
        {
            Key = "<<empty key>>";
            Name = "Get {Key}";
            Stack = new Input<Stack>();

            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
            Rules.Add(() => Stack.Step != null, "A stack is required.", "Stack");
            Rules.Add(() => !string.IsNullOrEmpty(Key), "The name of the output is required.", "Key");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            StackOutput so = OrchestrationService.ShowOutput(Stack.Value.StackName, Stack.Value.Id, Key);
            if(so != null)
            {
                Value = so.Value;
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }
}
