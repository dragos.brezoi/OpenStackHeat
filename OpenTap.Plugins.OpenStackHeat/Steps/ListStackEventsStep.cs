﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackHeat.Instruments;
using OpenTap.Plugins.OpenStackHeat.Objects;
using System.Collections.Generic;

namespace OpenTap.Plugins.OpenStackHeat.Steps
{
    [Display(Name: "List Stack Events", Description: "Lists events for a stack.", Group: "Heat")]
    public sealed class ListStackEventsStep : TestStep
    {
        #region Settings

        [Display(Name: "Orchestration Service", Group: "Context")]
        public OrchestrationServiceInstrument OrchestrationService { get; set; }

        [Display(Name: "Stack", Description: "The stack to list events for", Group: "Context")]
        public Input<Stack> Stack { get; set; }

        [Display(Name: "Events", Description: "Events for the stack.", Group: "Output")]
        [Output]
        public List<ResourceEvent> Events { get; set; }

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="ListStackEventsStep">ListStackEventsStep</see> class.
        /// </summary>
        public ListStackEventsStep()
        {
            Name = "List Stack Events";
            Stack = new Input<Stack>();

            Rules.Add(() => Stack.Step != null, "A stack is required.", "Stack");
            Rules.Add(() => OrchestrationService != null, "An Orchestration Service is required.", "OrchestrationService");
        }

        /// <summary>
        /// Run method is called when the step gets executed.
        /// </summary>
        public override void Run()
        {
            Events = OrchestrationService.ListStackEvents(Stack.Value.StackName, Stack.Value.Id);
            foreach (ResourceEvent @event in Events)
            {
                Log.Info(@event.ToString());
            }
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
