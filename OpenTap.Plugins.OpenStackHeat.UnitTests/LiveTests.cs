﻿//Copyright 2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using NUnit.Framework;
using OpenTap.Plugins.OpenStackHeat.Objects;
using OpenTap.Plugins.OpenStackHeat.RestClients;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackHeat.UnitTests
{
    [TestFixture]
    public class LiveTests
    {
        //[Test]
        public void TestIdentityAPIGetVersions()
        {
            string IdentityServiceEndpoint = "http://10.114.182.105:5000";
            Task<RestClientResult<List<Version>>> taskGetVersions = IdentityClient.GetVersions(IdentityServiceEndpoint);
            taskGetVersions.Wait();
            List<Version> versions = taskGetVersions.Result.Result;

            Assert.AreEqual(1, versions.Count);
            Assert.AreEqual("v3.11", versions[0].Id);
            Assert.IsNotNull(versions[0].Links);
            Assert.AreEqual("http://192.168.11.134:5000/v3/", versions[0].Links[0].HRef);
            Assert.AreEqual("self", versions[0].Links[0].Relation);
            Assert.AreEqual("stable", versions[0].Status);
            Assert.AreEqual("2018-10-15T00:00:00Z", versions[0].Updated);
        }

        //[Test]
        public void TestOrchestrationAPIGetVersions()
        {
            string OrchestrationServiceEndpoint = "http://10.114.182.105:8004";
            Task<RestClientResult<List<Version>>> taskGetVersions = OrchestrationClient.GetVersions(OrchestrationServiceEndpoint);
            taskGetVersions.Wait();
            List<Version> versions = taskGetVersions.Result.Result;

            Assert.AreEqual(1, versions.Count);
            Assert.AreEqual("v1.0", versions[0].Id);
            Assert.IsNotNull(versions[0].Links);
            Assert.AreEqual("http://10.114.182.105:8004/v1/", versions[0].Links[0].HRef);
            Assert.AreEqual("self", versions[0].Links[0].Relation);
            Assert.AreEqual("CURRENT", versions[0].Status);
            Assert.AreEqual(null, versions[0].Updated);
        }

        //[Test]
        public void TestAuthenticationAndRevocation()
        {
            string DomainName = "my_domain";
            string IdentityServiceEndpoint = "http://10.114.182.105:5000";
            //string OrchestrationServiceEndpoint = "http://10.114.182.105:8004";
            string ProjectId = "008dbb1590164528b197bac780288ce3";
            string User = "my_user";
            string Password = "my_password";

            Task<RestClientResult<string>> task1 = IdentityClient.Authenticate(IdentityServiceEndpoint, DomainName, ProjectId, User, Password);
            task1.Wait();
            Assert.IsNotNull("Token1 should not be null", task1.Result.Result);

            Task<RestClientResult<string>> task2 = IdentityClient.Authenticate(IdentityServiceEndpoint, DomainName, ProjectId, User, Password);
            task2.Wait();
            Assert.IsNotNull("Token2 should not be null", task2.Result.Result);

            Assert.AreNotEqual("Tokens not null, but are the same. Weird!!!", task1.Result.Result, task2.Result.Result);

            Task<RestClientResult<bool>> task3 = IdentityClient.RevokeToken(IdentityServiceEndpoint, task1.Result.Result);
            Task<RestClientResult<bool>> task4 = IdentityClient.RevokeToken(IdentityServiceEndpoint, task2.Result.Result);
            Thread.Sleep(500); // wait for the tokens to be deleted, to be replaced with something even based
            Task<RestClientResult<bool>> task5 = IdentityClient.RevokeToken(IdentityServiceEndpoint, "234234234234");
            Task<RestClientResult<bool>> task6 = IdentityClient.RevokeToken(IdentityServiceEndpoint, task2.Result.Result);

            bool revoked1 = task3.Result.Result;
            bool revoked2 = task4.Result.Result;
            bool revoked3 = task5.Result.Result;
            bool revoked4 = task6.Result.Result;

            Assert.IsTrue(revoked1, "Token1 was not revoked!");
            Assert.IsTrue(revoked2, "Token2 was not revoked!");
            Assert.IsFalse(revoked3, "Should not be possible to revoke an invalid token!");
            Assert.IsFalse(revoked4, "Should not be possible to revoke and already revoked token!");
        }
    }
}
